test("LeapYears", function (assert) {
    assert.equal(IsLeapYear(1980), true, "1980");
    assert.equal(IsLeapYear(2000), true, "2000");
    assert.equal(IsLeapYear(2004), true, "2004");
    assert.equal(IsLeapYear(2016), true, "2016");
    assert.equal(IsLeapYear(2020), true, "2020");
    assert.equal(IsLeapYear(2400), true, "2400")
});

test("NonLeapYearsExceptions", function (assert) {
    assert.equal(IsLeapYear(2100), false, "2100");
    assert.equal(IsLeapYear(2200), false, "2200");
    assert.equal(IsLeapYear(2300), false, "2300");
    assert.equal(IsLeapYear(1900), false, "1900");
    assert.equal(IsLeapYear(2500), false, "2500");
});

test("NonLeapYearsRegular", function (assert) {
    assert.equal(IsLeapYear(2017), false, "2017");
    assert.equal(IsLeapYear(2018), false, "2018");
    assert.equal(IsLeapYear(2019), false, "1019");
    assert.equal(IsLeapYear(2021), false, "2021");
    assert.equal(IsLeapYear(2001), false, "2001");
    assert.equal(IsLeapYear(2002), false, "2002");
    assert.equal(IsLeapYear(2005), false, "2005");
});

